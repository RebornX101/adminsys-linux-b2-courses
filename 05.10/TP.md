# I. Intro


- **|CLI|** afficher le nombre de services systemd dispos sur la machine
   ```bash
    $ sudo systemctl -t service -a | wc -l
   86
   ```
- **|CLI|** afficher le nombre de services systemd actifs et en cours   d'exécution ("running") sur la machine
   ```bash
   [vagrant@node1]~% sudo systemctl -t service | grep 'running' | wc -l
   17
   ```
- **|CLI|** afficher le nombre de services systemd qui ont échoué ("failed") ou qui sont inactifs ("exited") sur la machine
   ```bash
   [vagrant@node1 ~]$ sudo systemctl -t service | grep "failed\|exited" | wc  -l
   22

   ```
- **|CLI|** afficher la liste des services systemd qui démarrent    automatiquement au boot ("enabled")
   ```bash
   [vagrant@node1 ~]$ sudo systemctl list-unit-files -t service | grep    "enabled" | wc -l
   30
   ```


# II. Analyse d'un service

```bash
[vagrant@node1 ~]$ systemctl cat nginx
# /usr/lib/systemd/system/nginx.service
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
# Nginx will fail to start if /run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

- Path de l'unite ( prononce "unitai" stp j'ai pas de "e" accent aigue)
    > `/usr/lib/systemd/system/nginx.service`
- afficher son contenu et expliquer les lignes qui comportent :

     - ExecStart
        > Chemin de l'executable.
     - ExecStartPre
        > enchainement des commandes a passer avant de lancer le processus
     - PIDFile
        > ce qui va donner un PID au processus, creer un fichier "PID"
     - Type
        > "Unless Type=forking is set, the process started via this command line will be considered the main process of the daemon." - man systemd.service, 2020.
     - ExecReload
        > how to reload the service, here it's a kill of the process then re-executing the whole ExecStart Process (with ExecStartPre in it)
     - Description
        > really?
     - After 
        > what to do after the process is up and running

# III. Creation d'un service

 - Créez une unité de service qui lance un serveur web:
   > voir server.service

 ### A.Lancer le service
- prouver qu'il est en cours de fonctionnement pour systemd
   ```bash
     [vagrant@node3 ~]$ sudo systemctl status server
   ● server.service - Starts a web server
      Loaded: loaded (/etc/systemd/system/server.service; disabled; vendor    preset: disabled)
      Active: active (running) since Wed 2020-10-07 00:02:04 UTC; 5s ago
     Process: 25479 ExecStartPre=/usr/bin/firewall-cmd --add-port=${PORT}/tcp    (code=exited, status=0/SUCCESS)
     Process: 25475 ExecStartPre=/usr/bin/firewall-cmd --add-port=${PORT}/tcp    --permanent (code=exited, status=0/SUCCESS)
    Main PID: 25483 (python2)
      CGroup: /system.slice/server.service
              └─25483 /usr/bin/python2 -m SimpleHTTPServer 6969

   ```

- faites en sorte que le service s'allume au démarrage de la machine
   ```bash
   $ sudo systemctl enable server.service
   Created symlink from /etc/systemd/system/multi-user.target.wants/server.   service to /etc/systemd/system/server.service.

   ```

- prouver que le serveur web est bien fonctionnel
   ```bash
   [vagrant@node3 ~]$ curl -L localhost:6969
   <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
   <title>Directory listing for /</title>
   <body>
   <h2>Directory listing for /</h2>
   [...]
   <li><a href="var/">var/</a>
   </ul>
   <hr>
   </body>
   </html>
   ```
#### B. Sauvegarde

# II. Autres Features

#### A. Gestion de boot

##### les 3 services:
 1. firewalld.service (814ms)
 2. dev-sda1.device (521ms)
 3. postfix.service (459ms)

#### B. Gestion de l'heure
   
   1. Synchro
   ```bash
   [vagrant@node3 ~]$ sudo timedatectl
      Local time: Wed 2020-10-07 15:43:33 UTC
     Universal time: Wed 2020-10-07 15:43:33 UTC
        RTC time: Wed 2020-10-07 15:43:32
       Time zone: UTC (UTC, +0000)
     NTP enabled: yes
   NTP synchronized: yes
   ```
   >>>> `NTP synchronized: yes`
   
   2. Changer de Timezone:
   ```bash
   [vagrant@node3 ~]$ sudo timedatectl set-timezone Europe/Paris
   [vagrant@node3 ~]$ date
   Wed Oct  7 17:52:15 CEST 2020
   [vagrant@node3 ~]$ 
   ```
   3. Change the Hostname
   ```bash
   [vagrant@node3 ~]$ sudo hostnamectl set-hostname "adminsys"
   [vagrant@node3 ~]$ sudo hostnamectl
   Static hostname: adminsys
   Icon name: computer-vm
   Chassis: vm
   ...
   ...
   ```