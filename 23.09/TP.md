# TP1 : Déploiement classique


- [TP1 : Déploiement classique](#tp1--déploiement-classique)
- [0. Prérequis](#0-prérequis)
- [I. Setup serveur Web](#i-setup-serveur-web)
- [II. Script de sauvegarde](#ii-script-de-sauvegarde)
    - [Creation utilisateur:](#creation-utilisateur)
- [III. Monitoring, alerting](#iii-monitoring-alerting)

<!-- vim-markdown-toc -->

# 0. Prérequis

 **Setup de deux machines CentOS7 configurée de façon basique.**
* partitionnement
    ```bash
        [user@localhost ~]$ lsblk
        NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        sda               8:0    0    8G  0 disk 
        ├─sda1            8:1    0    1G  0 part /boot
        └─sda2            8:2    0    7G  0 part 
          ├─centos-root 253:0    0  6.2G  0 lvm  /
          └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
        sdb               8:16   0    5G  0 disk 
        sr0              11:0    1 1024M  0 rom  
    ```
  * partitionner le nouveau disque avec LVM
    ```bash
    [user@localhost ~]$ sudo pvcreate /dev/sdb
    Physical volume "/dev/sdb" successfully created.


    [user@localhost ~]$ sudo pvs
    PV         VG     Fmt  Attr PSize  PFree
    /dev/sda2  centos lvm2 a--  <7.00g    0 
    /dev/sdb          lvm2 ---   5.00g 5.00g

    [user@localhost ~]$ sudo vgcreate site /dev/sdb
    [sudo] password for user: 
    Volume group "site" successfully created
    
    [user@localhost ~]$ sudo vgs
    VG     #PV #LV #SN Attr   VSize  VFree 
    centos   1   2   0 wz--n- <7.00g     0 
    site     1   0   0 wz--n- <5.00g <5.00g

    ```
    * deux partitions, une de 2Go, une de 3Go
    ```bash
    [user@localhost ~]$ sudo lvcreate -L 2G site -n site1
    Logical volume "site1" created.
    [user@localhost ~]$ sudo lvcreate -l 100%FREE site -n site2
      Logical volume "site2" created.
    [user@localhost ~]$ sudo lvs
      LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log    Cpy%Sync Convert
      root  centos -wi-ao----  <6.  20g                                                    
      swap  centos -wi-ao---- 820.  00m                                                    
      site1 site   -wi-a-----   2.  00g                                                    
      site2 site   -wi-a-----  <3.00g   
    ```
    * la partition de 2Go sera montée sur `/srv/site1` / 
    * la partition de 3Go sera montée sur `/srv/site2`

    ```bash
    [user@localhost ~]$ sudo !!
    sudo mkfs -t ext4 /dev/site/site1
    mke2fs 1.42.9 (28-Dec-2013)
    Filesystem label=
    OS type: Linux
    Block size=4096 (log=2)
    Fragment size=4096 (log=2)
    Stride=0 blocks, Stripe width=0 blocks
    131072 inodes, 524288 blocks
    26214 blocks (5.00%) reserved for the super user
    First data block=0
    Maximum filesystem blocks=536870912
    16 block groups
    32768 blocks per group, 32768 fragments per group
    8192 inodes per group
    Superblock backups stored on blocks: 
    	32768, 98304, 163840, 229376, 294912

    Allocating group tables: done                            
    Writing inode tables: done                            
    Creating journal (16384 blocks): done
    Writing superblocks and filesystem accounting information: done 




    [user@localhost ~]$ sudo mkfs -t ext4 /dev/site/site2
    mke2fs 1.42.9 (28-Dec-2013)
    Filesystem label=
    OS type: Linux
    Block size=4096 (log=2)
    Fragment size=4096 (log=2)
    Stride=0 blocks, Stripe width=0 blocks
    196608 inodes, 785408 blocks
    39270 blocks (5.00%) reserved for the super user
    First data block=0
    Maximum filesystem blocks=805306368
    24 block groups
    32768 blocks per group, 32768 fragments per group
    8192 inodes per group
    Superblock backups stored on blocks: 
    	32768, 98304, 163840, 229376, 294912

    Allocating group tables: done                            
    Writing inode tables: done                            
    Creating journal (16384 blocks): done
    Writing superblocks and filesystem accounting information: done 

    [user@localhost ~]$ sudo mount /dev/site/site1 /srv/site1 
    [user@localhost ~]$ sudo mount /dev/site/site2 /srv/site2

    [user@localhost ~]$ mount | grep "site"
    /dev/mapper/site-site1 on /srv/site1 type ext4 (rw,relatime,seclabel,data=ordered)
    /dev/mapper/site-site2 on /srv/site2 type ext4 (rw,relatime,seclabel,data=ordered)


    ```
  * les partitions doivent être montées automatiquement au démarrage (fichier `/etc/fstab`)
    ```vim
    # /etc/fstab
    # Created by anaconda on Wed Sep 23 08:50:29 2020
    #
    # Accessible filesystems, by reference, are maintained under '/dev/disk'
    # See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
    #
    /dev/mapper/centos-root /                       xfs     defaults        0 0
    UUID=f6cd7edf-7d76-4932-9768-34f321bc5c97 /boot                   xfs       defaults        0 0
    /dev/mapper/centos-swap swap                    swap    defaults        0 0

    # 2nd LV of SITE1 & SITE2 on /srv/site~

    /dev/mapper/site-site1 /srv/site1 ext4 defaults 0 0
    /dev/mapper/site-site2 /srv/site2 ext4 defaults 0 0

    ```

    ```bash

    [user@localhost ~]$ sudo mount -av
    /                        : ignored
    /boot                    : already mounted
    swap                     : ignored
    mount: /srv/site1 does not contain SELinux labels.
           You just mounted an file system that supports labels which does not
           contain labels, onto an SELinux box. It is likely that confined
           applications will generate AVC messages and not be allowed access to
           this file system.  For more details see restorecon(8) and mount(8).
    /srv/site1               : successfully mounted
    mount: /srv/site2 does not contain SELinux labels.
           You just mounted an file system that supports labels which does not
           contain labels, onto an SELinux box. It is likely that confined
           applications will generate AVC messages and not be allowed access to
           this file system.  For more details see restorecon(8) and mount(8).
    /srv/site2               : successfully mounted


    ```
* un accès internet
  * carte réseau dédiée
  * route par défaut
    ```bash
    [user@localhost ~]$ ip route show
    default via 10.0.2.2 dev enp0s3 proto dhcp metric 101 
    10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 101 
    192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.11 metric 100 
    ```
* un accès à un réseau local (les deux machines peuvent se `ping`) 
  * carte réseau dédiée 
  * route locale
    ```bash
    [user@localhost ~]$ ping 192.168.1.12
    PING 192.168.1.12 (192.168.1.12) 56(84) bytes of data.
    64 bytes from 192.168.1.12: icmp_seq=1 ttl=64 time=1.71 ms
    64 bytes from 192.168.1.12: icmp_seq=2 ttl=64 time=1.05 ms
    64 bytes from 192.168.1.12: icmp_seq=3 ttl=64 time=1.11 ms
    64 bytes from 192.168.1.12: icmp_seq=4 ttl=64 time=1.23 ms
    ^C
    --- 192.168.1.12 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3007ms
    rtt min/avg/max/mdev = 1.056/1.280/1.716/0.261 ms

    ```
* les machines doivent avoir un nom 
  * `/etc/hostname` 
  * commande `hostname`)
* les machines doivent pouvoir se joindre par leurs noms respectifs 
  * fichier `/etc/hosts`
    ```bash
    [user@node2]~% sudo vim /etc/hosts
    [user@node2]~% ping node1.tp1.b2
    PING node1.tp1.b2 (192.168.1.11) 56(84) bytes of data.
    64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=1 ttl=64 time=0.416 ms
    64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=2 ttl=64 time=1.36 ms
    64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=3 ttl=64 time=1.34 ms
    64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=4 ttl=64 time=1.35 ms
    ^C
    --- node1.tp1.b2 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3008ms
    rtt min/avg/max/mdev = 0.416/1.118/1.365/0.407 ms

    
    ```
* un utilisateur administrateur est créé sur les deux machines (il peut exécuter des commandes `sudo` en tant que `root`)
  * création d'un user
  * modification de la conf sudo
* vous n'utilisez QUE `ssh` pour administrer les machines
  * création d'une paire de clés (sur VOTRE PC)
  * déposer la clé publique sur l'utilisateur de destination
* le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires
  * commande `firewall-cmd` ou `iptables`

Pour le réseau des différentes machines :

| Name           | IP             |
|----------------|----------------|
| `node1.tp1.b2` | `192.168.1.11` |
| `node2.tp1.b2` | `192.168.1.12` |

# I. Setup serveur Web

🌞 Installer le serveur web NGINX sur `node1.tp1.b2` (avec une commande `yum install`).

```bash
[user@node1]~% sudo yum install nginx        
Loaded plugins: fastestmirror

[...]
Is this ok [y/d/N]: y
Downloading packages:
nginx-1.18.0-1.el7.ngx.x86_64.rpm                             | 772 kB  00:00:00     
[...]
Warning: RPMDB altered outside of yum.
  Installing : 1:nginx-1.18.0-1.el7.ngx.x86_64                                   1/1 
----------------------------------------------------------------------

Thanks for using nginx!
[...]

----------------------------------------------------------------------
  Verifying  : 1:nginx-1.18.0-1.el7.ngx.x86_64                                   1/1 

Installed:
  nginx.x86_64 1:1.18.0-1.el7.ngx                                                    

Complete!

```

🌞 Faites en sorte que :
* NGINX servent deux sites web, chacun possède un fichier unique `index.html`
* les sites web doivent se trouver dans `/srv/site1` et `/srv/site2`
  * les permissions sur ces dossiers doivent être le plus restrictif possible
  * ces dossiers doivent appartenir à un utilisateur et un groupe spécifique
    ```bash
    [user@node1]/srv/site2% ls -la
    total 24
    drwxr--r--. 3 user root  4096   Sep 24 04:44 .
    drwxr-xr-x. 5 root root    44   Sep 23 11:13 ..
    -rwxr--r--. 1 server server  51 Sep 23 12:37 index.html
    drwx------. 2 root root 16384 Sep 23 11:25 lost+found

    [user@node1]/srv% ls -la
    total 8
    drwxr-xr-x.  5 root   root   44 Sep 23 11:13 .
    dr-xr-xr-x. 17 root   root  224 Sep 23 08:57 ..
    drwxr-xr-x.  2 root   root    6 Sep 23 10:27 data
    drwxr--r--.  3 server user 4096 Sep 24 05:31 site1
    drwxr--r--.  3 server user 4096 Sep 24 05:32 site2

    ```

* NGINX doit utiliser un utilisateur dédié que vous avez créé à cet effet
* les sites doivent être servis en HTTPS sur le port 443 et en HTTP sur le port 80
  * n'oubliez pas d'ouvrir les ports firewall

    ```bash
    [user@node1]/srv% sudo firewall-cmd --add-port=80/tcp
    success
    [user@node1]/srv% sudo firewall-cmd --add-port=403/tcp
    success
    [user@node1]/srv% sudo firewall-cmd --reload
    success
    ```

Voici un exemple d'une unique fichier de configuration `nginx.conf` qui ne sert qu'un seul site, sur le port 8080, se trouvant dans `/tmp/test`:

```
worker_processes 1;
error_log nginx_error.log;
events {
    worker_connections 1024;
}

http {
    server {
        listen 8080;

        location / {
            root /tmp/test;
        }
    }
}
```

🌞 Prouver que la machine `node2` peut joindre les deux sites web.

```bash
[user@node2]~% curl -L node1.tp1.b2
<html>
<head><title>403 Forbidden</title></head>
<body>
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.18.0</center>
</body>
</html>
```

>> CA MARCHE BORDEL HOLY SHIT

# II. Script de sauvegarde

**Yup. Again.**

🌞 Ecrire un script qui :
* s'appelle `tp1_backup.sh`
* sauvegarde les deux sites web
  * c'est à dire qu'il crée une archive compressée pour chacun des sites
  * je vous conseille d'utiliser le format `tar` pour l'archivage et `gzip` pour la compression
* les noms des archives doivent contenir le nom du site sauvegardé ainsi que la date et heure de la sauvegarde
  * par exemple `site1_20200923_2358` (pour le 23 Septembre 2020 à 23h58)
* vous ne devez garder que 7 exemplaires sauvegardes
  * à la huitième sauvegarde réalisée, la plus ancienne est supprimée
* le script ne sauvegarde qu'un dossier à la fois, le chemin vers ce dossier est passé en argument du script
  * on peut donc appeler le script en faisant `tp1_backup.sh /srv/site1` afin de déclencher une sauvegarde de `/srv/site1`

```bash
#!/bin/bash
# Sam ADONE
# 28/09/2020
# Simple website backup script

# first brgrsite to backup
SITE=$1

UserID=1003

# fetching date and time
BACKUPTIME=`date +%Y%m%d_%H%M%S` #get the current date

# concatenate directory name + date & time
SiteDateTime="${SITE}_${BACKUPTIME}"


BackupQtty=7
NbOfFiles=$(ls backup/ | wc -l)
OldFiles=$(ls backup/ | tail -1)

## target dir
# TargetDir =$(echo "${target_path$%/}" | rev | cut -d '/' -f1 | rev)

# User checking

if [[ ${EUID} -ne ${UserID} ]]
then
        echo "HEHE, WRONG USER, YOU DO NOT HAVE THE RIGHTS"
        exit 1
fi

# destination folder
echo "BACKUP IN PROGRESS"
echo "================="
DESTINATION=backup/$SiteDateTime

archiveCompress () {

tar -czf "${DESTINATION}" "${SITE}"

if [[ $? -eq 0 ]]
then
    echo "Backup Completed"
else
    echo "backup failed" >&2
fi
}

MaxSave() {

if [[ ${NbOfFiles} -gt ${BackupQtty} ]]
then
        cd backup/
        rm -f ${OldFiles}
        cd ..
fi

}
echo ${OldFiles}
MaxSave
archiveCompress "${SITE}"



echo "================="
echo "Backup complete (if no errors above)"

              
```
### Creation utilisateur:
```bash
user@node1 ~]$ sudo useradd backup
[sudo] password for user: 
[user@node1 ~]$ id backup
uid=1003(backup) gid=1003(backup) groups=1003(backup)
[user@node1 ~]$ sudo passwd -f -u backup
Unlocking password for user backup.
passwd: Success
```

🌞 Utiliser la `crontab` pour que le script s'exécute automatiquement toutes les heures.
```bash
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root
#
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7)
# |  |  |  |  |
# *  *  *  *  * command to be executed
0  */1  *  *  *   su backup ./srv/tp1_backup.sh site1
0  */1  *  *  *   su backup ./srv/tp1_backup.sh site2
```

🌞 Prouver que vous êtes capables de restaurer un des sites dans une version antérieure, et fournir une marche à suivre pour restaurer une sauvegarde donnée.

**NB** : votre script 

* doit s'exécuter sous l'identité d'un utilisateur dédié appelé `backup`
  
* ne doit comporter **AUCUNE** commande `sudo`

* doit posséder des permissions minimales à son bon fonctionnement

* doit utiliser des variables et des fonctions, **avec des noms explicites**

🐙 Créer une unité systemd qui permet de déclencher le script de backup
* c'est à dire, faire en sorte que votre script de backup soit déclenché lorsque l'on exécute `sudo systemctl start backup`

```bash
[user@node1 srv]$ sudo cat /etc/systemd/system/backup.service 
~
[Unit]
Description=Site backup service
After=network.target
StartLimitIntervalSec=0
~
[Service]
Type=simple
ExecStart=/srv/tp1_backup.sh site1 site2
~
[Install]
WantedBy=multi-user.target
~
~
[user@node1]/srv% sudo systemctl start backup.service
```

# III. Monitoring, alerting

🌞 Mettre en place l'outil Netdata en suivant [les instructions officielles](https://learn.netdata.cloud/docs/agent/packaging/installer) et s'assurer de son bon fonctionnement.

🌞 Configurer Netdata pour qu'ils vous envoient des alertes dans un salon Discord dédié
* c'est à dire que Netdata vous informera quand la R:
* RAM est pleine, ou le disque, ou autre, *via* Discord


```bash
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```