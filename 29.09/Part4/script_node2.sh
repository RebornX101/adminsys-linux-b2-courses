#!/bin/bash


# starting the firewall as asked
systemctl start firewalld
setenforce 0
firewall-cmd --reload


# adding admin as a user
useradd admin
echo 'admin' | passwd --stdin admin

# adding the hosts "node1"
echo ' 192.168.1.11 node1.tp2.b2' | tee /etc/hosts

# balek du certificat
echo -n | openssl s_client -connect node1.tp2.b2:443 \
    | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /etc/pki/ca-trust/source/anchors/server.cert
update-ca-trust