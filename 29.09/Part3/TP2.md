# Multi deployement

```ruby
sam@somewhere [15:44:19] [~/disk2/Documents/Ynov/b2_Adminsys_linux/adminsys-linux-b2-courses/29.09/Part3] [master *]
-> % cat Vagrantfile 
# -*- mode: ruby -*-
# vi: set ft=ruby :


Vagrant.configure("2") do |config|
  # Common config
  config.vm.box = "b2-tp2-centos"

  # "node1" only config
  config.vm.define "node1" do |node1|
    node1.vm.provider "virtualbox" do |vb1|
      vb1.customize ["modifyvm", :id, "--memory", "1024"]
    end
    # node1 on the network
    node1.vm.network "private_network", ip: "192.168.56.11"
    node1.vm.hostname = "node1.tp2.b2"
  end

  # "node2" only
  config.vm.define "node2" do |node2|
    node2.vm.provider "virtualbox" do |vb2|
      vb2.customize ["modifyvm", :id, "--memory", "512"]
    end
    # node2 on the network
    node2.vm.network "private_network", ip: "192.168.56.12"
    node2.vm.hostname = "node2.tp2.b2"
  end
end

```