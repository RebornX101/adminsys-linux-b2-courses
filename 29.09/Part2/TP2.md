# II. Re-package

 - une mise à jour système

        - yum update


- l'installation de paquets additionels

-   - vim
    - epel-release
    - nginx


- désactivation de SELinux
    firewall (avec firewalld, en utilisant la commande firewall-cmd)
    activé au boot de la VM
    ne laisse passser que le strict nécessaire (SSH)
    
    >bah euh ui: 
    ```bash
    [vagrant@node1 ~]$ sudo firewall-cmd --list-port
    22/tcp

    [vagrant@node1 ~]$ sestatus
    SELinux status:                 disabled
    SELinuxfs mount:                /sys/fs/selinux
    SELinux root directory:         /etc/selinux
    Loaded policy name:             targeted
    Current mode:                   permissive
    Mode from config file:          disabled
    Policy MLS status:              enabled
    Policy deny_unknown status:     allowed
    Max kernel policy version:      31

    ```

### centos.7.vagrant.fitgirl.repack.exe

    ```bash
    sam@somewhere [15:04:10] [~/disk2/Documents/Ynov/b2_Adminsys_linux/adminsys-linux-b2-courses/29.09/Part2] [master *]
    -> % vagrant package --output b2-tp2-centos.    box                                                           [master ●]
    ==> default: Attempting graceful shutdown of VM...
    ==> default: Clearing any previously set forwarded ports...
    ==> default: Exporting VM...
    ==> default: Compressing package to: /home/sam/disk2/Documents/Ynov/    b2_Adminsys_linux/adminsys-linux-b2-courses/29.09/Part2/b2-tp2-centos.box

    sam@somewhere ~/disk2/Documents/Ynov/b2_Adminsys_linux/ adminsys-linux-b2-courses/29.09/Part2              [15:07:03]
    sam@somewhere [15:08:25] [~/disk2/Documents/Ynov/b2_Adminsys_linux/adminsys-linux-b2-courses/29.09/Part2] [master *]
    -> % vagrant box add b2-tp2-centos b2-tp2-centos.   box                                                     [master ●●]
    
    ==> box: Box file was not detected as metadata. Adding it directly...
    ==> box: Adding box 'b2-tp2-centos' (v0) for provider: 
        box: Unpacking necessary files from: file:///home/sam/disk2/Documents/  Ynov/b2_Adminsys_linux/adminsys-linux-b2-courses/29.09/Part2/ b2-tp2-centos.box
    ==> box: Successfully added box 'b2-tp2-centos' (v0) for 'virtualbox'!

    ```