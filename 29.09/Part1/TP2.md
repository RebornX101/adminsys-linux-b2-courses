# 0 Prerequiredededededededededed

enfaite y'a rien 

# I. Déploiement simple

### VagrantFile:
```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  ## Les 3 lignes suivantes permettent d'éviter certains bugs et/ou d'accélérer le déploiement. Gardez-les tout le temps sauf contre-indications.
  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false
  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 
  
  # Creates a Hsot-only network access to the machine
  # using a specific IP
  config.vm.network "private_network", ip: "192.168.1.11"

  # Provider-specific configuration to fine-tune the VM:
  config.vm.provider "virtualbox" do |vb|
    # displays the VB gui when booting the machine:
    vb.gui = false
    # customises the amount of memory
    vb.memory = "1024"
    # define Name (for Vagrant) and Hostname:
    config.vm.hostname = "node1.tp2.b2"
    vb.name = "VG-node1"
  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true
end
```

### Demarrage de la machineu

```bash

✘ sam@somewhere  ~/Programs/Vagrant/b2_TP2  vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: VG-node1
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: hostonly
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: 
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default: 
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
[...]
==> default: Setting hostname...
==> default: Configuring and enabling network interfaces...
```
### script vim
```shell
#!/bin/sh

sudo yum install vim -y
```
avec le rajout de la ligne dans le Vagrantfile
```ruby
  config.vm.provision "shell", path: "~/Programs/Vagrant/b2_TP2/script.sh"
```

### Ajout 2nd disk:
Vagrantfile <3
```ruby
    # Crée le disque, uniquement s'il nexiste pas déjà
    unless File.exist?(CONTROL_NODE_DISK)
      vb.customize ['createhd', '--filename', CONTROL_NODE_DISK, '--variant', 'Fixed', '--size', 5 * 1024]
    end

    # Attache le disque à la VM
    vb.customize ['storageattach', :id,  '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', CONTROL_NODE_DISK]
```
```bash
sam@somewhere ~/Programs/Vagrant/b2_TP2 ~> vagrant ssh                                         
    x[vagrant@node1 ~]$ lsblk
    NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
    sda      8:0    0  40G  0 disk 
    └─sda1   8:1    0  40G  0 part /
    sdb      8:16   0   5G  0 disk 
```