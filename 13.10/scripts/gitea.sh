# Gitea installation on the node from the binaries


cd

# required dependencies
sudo yum install -y wget

sudo yum install -y git


# Gitea installation process:
wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64

sudo chmod +x gitea

sudo adduser --system --shell /bin/bash --home /home/git git

sudo mkdir -p /var/lib/gitea/{custom,data,log}

sudo usermod -a -G git git

sudo chown -R git:git /var/lib/gitea

sudo chmod -R 750 /var/lib/gitea/

sudo mkdir /etc/gitea

sudo chown root:git /etc/gitea

chmod 770 /etc/gitea

# Now setting up the environment variable

export GITEA_WORK_DIR=/var/lib/gitea/

# Copy gitea binary to global location

sudo cp gitea /usr/local/bin/gitea

# now setting a Gitea Service: 

sudo touch /etc/systemd/system/gitea.service

sudo echo "[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
Requires=mariadb.service

[Service]
RestartSec=3s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/gitea.service
sudo systemctl enable gitea

# launching Gitea!
sudo GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini
