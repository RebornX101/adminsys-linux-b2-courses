# this is a script to set up all nodes
## this script is common to all nodes
###  SELinux/Hosts/etc
# this is a macro


# Adding hosts to /etc/hosts
echo 'Writing Hosts in /etc/hosts folder...
...
...'
sudo echo '
# Host		Address
192.168.4.21	gitea.tp4.b2
192.168.4.22	mariadb.tp4.b2
192.168.4.23	nginx.tp4.b2
192.168.4.24	nfs.tp4.b2' >> /etc/hosts
echo 'Complete! :D '

# installation of vim
echo 'installing vim'
sudo yum install -y vim
echo 'Vim INSTALLED EH!'

# Dealing with SELinux
echo 'Disabling SELinux'
echo '...'
sudo setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config
echo '...'
echo 'SELINUX disabled'



